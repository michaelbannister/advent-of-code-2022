package main

import (
	"aoc2022/util"
	"math"
	"os"
)

func main() {
	util.RunDay(12, part1, part2)
}

func part1(file *os.File) {
	grid := parseGrid(file)

	shortestPath := solve(&grid, findStart(&grid))
	println("part 1:", shortestPath)
}

func part2(file *os.File) {
	grid := parseGrid(file)
	shortest := solve(&grid, findStart(&grid))

	// just brute-force it!

	candidates := make([]*square, 0)
	for _, row := range grid {
		for _, sq := range row {
			if sq.elevation == 'a' {
				candidates = append(candidates, sq)
			}
		}
	}
	for _, sq := range candidates {
		reset(&grid)
		dist := solve(&grid, sq)
		if dist < shortest {
			shortest = dist
		}
	}

	println("part 2:", shortest)
}

func reset(grid *[][]*square) {
	for _, row := range *grid {
		for _, sq := range row {
			sq.dist = MaxDistance
			sq.visited = false
		}
	}
}

func parseGrid(file *os.File) [][]*square {
	grid := make([][]*square, 0)

	x := 0
	for line := range util.Lines(file) {
		width := len(line)
		row := make([]*square, width)
		for y := 0; y < width; y++ {
			sq := &square{elevation: line[y], dist: MaxDistance, x: x, y: y}
			row[y] = sq
		}
		grid = append(grid, row)
		x++
	}
	return grid
}

const MaxDistance = math.MaxInt / 2

type square struct {
	elevation byte
	dist      int
	visited   bool
	x, y      int
}

func solve(grid *[][]*square, start *square) int {
	start.dist = 0

	current := start
	for true {
		neighbours := current.neighbours(grid)
		for _, neighbour := range neighbours {
			if neighbour.visited {
				continue
			}

			newDistance := current.dist + current.distanceTo(neighbour)
			if newDistance < MaxDistance && newDistance < neighbour.dist {
				neighbour.dist = newDistance
			}
		}
		current.visited = true
		next := nextUnvisited(grid)
		if next.elevation == 'E' {
			next.dist = current.dist + current.distanceTo(next)
			current = next
			break
		}
		current = next
	}
	return current.dist
}

func nextUnvisited(grid *[][]*square) *square {
	var next *square
	for _, row := range *grid {
		for _, sq := range row {
			if sq.visited {
				continue
			}
			if next == nil || sq.dist < next.dist || (sq.dist == next.dist && sq.elevation == 'E') {
				next = sq
			}
		}
	}
	return next
}

func findStart(grid *[][]*square) *square {
	for _, row := range *grid {
		for _, sq := range row {
			if sq.elevation == 'S' {
				return sq
			}
		}
	}
	panic("Start not found")
}

func (s *square) neighbours(grid *[][]*square) []*square {
	result := make([]*square, 0)
	if s.x-1 >= 0 {
		result = append(result, (*grid)[s.x-1][s.y])
	}
	if s.x+1 < len(*grid) {
		result = append(result, (*grid)[s.x+1][s.y])
	}
	if s.y-1 >= 0 {
		result = append(result, (*grid)[s.x][s.y-1])
	}
	if s.y+1 < len((*grid)[s.x]) {
		result = append(result, (*grid)[s.x][s.y+1])
	}
	return result
}

func (s *square) distanceTo(other *square) int {
	from := s.elevation
	if from == 'S' {
		from = 'a'
	}
	to := other.elevation
	if to == 'E' {
		to = 'z'
	}

	if to > (from + 1) {
		return MaxDistance
	} else {
		return 1
	}
}
