package main

import (
	"aoc2022/util"
	"os"
	"strconv"
)

func main() {
	util.RunDay(10, part1, part2)
}

func part1(file *os.File) {
	xs := runProgram(file)

	sum := 0
	for c := 20; c <= 220; c += 40 {
		// value 'during' the cycle is the value at the end of the previous cycle
		sum += xs[c-1] * c
	}

	println("part 1:", sum)
}

func part2(file *os.File) {
	xs := runProgram(file)
	println("part 2:")
	for c, sprite := range xs {
		pixel := c % 40
		if sprite-1 <= pixel && pixel <= sprite+1 {
			print("#")
		} else {
			print(".")
		}
		if pixel == 39 {
			println()
		}
	}

}

func runProgram(file *os.File) []int {
	x := make([]int, 300)
	x[0] = 1

	cycle := 0
	for line := range util.Lines(file) {
		if line == "noop" {
			currentX := x[cycle]
			cycle++
			x[cycle] = currentX
		} else if line[0:4] == "addx" {
			change, _ := strconv.Atoi(line[5:])
			currentX := x[cycle]
			cycle++
			x[cycle] = currentX
			cycle++
			x[cycle] = currentX + change
		}
	}
	return x
}
