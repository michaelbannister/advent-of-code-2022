package main

import "testing"

func Test_cave_addSand(t *testing.T) {
	c := cave{inlet: point{500, 0}}
	c.rockAt(498, 4)
	c.rockAt(498, 5)
	c.rockAt(498, 6)
	c.rockAt(497, 6)
	c.rockAt(496, 6)

	c.rockAt(503, 4)
	c.rockAt(502, 4)
	c.rockAt(502, 5)
	c.rockAt(502, 6)
	c.rockAt(502, 7)
	c.rockAt(502, 8)
	c.rockAt(502, 9)
	c.rockAt(501, 9)
	c.rockAt(500, 9)
	c.rockAt(499, 9)
	c.rockAt(498, 9)
	c.rockAt(497, 9)
	c.rockAt(496, 9)
	c.rockAt(495, 9)
	c.rockAt(494, 9)

	c.addAllSand()

	if c.sandCount != 24 {
		t.Errorf("added %v sand, expected 24", c.sandCount)
	}
}
