package main

import (
	"aoc2022/util"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	util.RunDay(14, part1, part2)
}

func part1(file *os.File) {
	c := parseCave(file)

	c.addAllSand()

	println("part 1:", c.sandCount)
}

func parseCave(file *os.File) cave {
	c := cave{inlet: point{500, 0}}
	for line := range util.Lines(file) {
		coords := strings.Split(line, " -> ")
		points := make([]point, len(coords))
		for i, coord := range coords {
			xystrs := strings.Split(coord, ",")
			x, _ := strconv.Atoi(xystrs[0])
			y, _ := strconv.Atoi(xystrs[1])
			points[i] = point{x, y}
		}

		for i := 0; i < len(points)-1; i++ {
			p1, p2 := points[i], points[i+1]
			for _, p := range p1.rangeTo(p2) {
				c.rockAt(p.x, p.y)
			}
		}
	}
	return c
}

func part2(file *os.File) {
	c := parseCave(file)
	c.hasFloor = true

	c.addAllSand()

	println("part 2:", c.sandCount)
}

const rock = '#'

const sand = 'o'

type point struct{ x, y int }

func (p point) String() string {
	return fmt.Sprintf("(%v,%v)", p.x, p.y)
}

func (p point) fallPoints() []point {
	points := []point{
		{p.x, p.y + 1},
		{p.x - 1, p.y + 1},
		{p.x + 1, p.y + 1},
	}
	return points
}

func (p point) rangeTo(p2 point) []point {
	var result []point
	if p.x == p2.x {
		if p.y > p2.y {
			p, p2 = p2, p
		}
		length := p2.y - p.y + 1
		result = make([]point, length)
		for i := 0; i < length; i++ {
			result[i] = point{p.x, p.y + i}
		}
	} else if p.y == p2.y {
		if p.x > p2.x {
			p, p2 = p2, p
		}
		length := p2.x - p.x + 1
		result = make([]point, length)
		for i := 0; i < length; i++ {
			result[i] = point{p.x + i, p.y}
		}
	} else {
		panic("Points must have same x or same y")
	}
	return result
}

type cave struct {
	grid      map[point]byte
	inlet     point
	maxY      int
	sandCount int
	hasFloor  bool
}

func (c *cave) addAllSand() {
	for c.addSand() && c.isEmpty(c.inlet) {
		// repeat until false
	}
}

// addSand introduces a new unit of sand and resolves its fall.
// Returns true if the sand comes to rest in the cave; false if it fell into the abyss
func (c *cave) addSand() bool {
	s := c.inlet

	for !c.isAbyss(s) {
		fallen := false
		for _, p := range s.fallPoints() {
			if c.isEmpty(p) {
				s = p
				fallen = true
				break
			}
		}
		if !fallen {
			// nowhere to fall to
			c.sandStoppedAt(s)
			return true
		}
	}
	return false
}

func (c *cave) isEmpty(p point) bool {
	if c.hasFloor && p.y >= c.maxY+2 {
		return false
	}
	_, found := c.grid[p]
	return !found
}

func (c *cave) isAbyss(p point) bool {
	if c.hasFloor {
		return false
	} else {
		return p.y > c.maxY
	}
}

func (c *cave) sandStoppedAt(p point) {
	c.grid[p] = sand
	c.sandCount++
}

func (c *cave) rockAt(x, y int) {
	if c.grid == nil {
		c.grid = make(map[point]byte)
	}
	c.grid[point{x, y}] = rock
	if y > c.maxY {
		c.maxY = y
	}
}
