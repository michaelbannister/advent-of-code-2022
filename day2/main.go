package main

import (
	"aoc2022/util"
	"os"
)

const win = 6
const draw = 3
const lose = 0

func main() {
	util.RunDay(2, part1, part2)
}

func part1(file *os.File) {
	score := map[byte]map[byte]int{
		'A': {
			'X': draw + 1,
			'Y': win + 2,
			'Z': lose + 3,
		},
		'B': {
			'X': lose + 1,
			'Y': draw + 2,
			'Z': win + 3,
		},
		'C': {
			'X': win + 1,
			'Y': lose + 2,
			'Z': draw + 3,
		},
	}

	total := totalScore(file, score)

	println("part 1:", total)
}

func part2(file *os.File) {
	score := map[byte]map[byte]int{
		'A': {
			'Y': draw + 1,
			'Z': win + 2,
			'X': lose + 3,
		},
		'B': {
			'X': lose + 1,
			'Y': draw + 2,
			'Z': win + 3,
		},
		'C': {
			'Z': win + 1,
			'X': lose + 2,
			'Y': draw + 3,
		},
	}

	total := totalScore(file, score)

	println("part 2:", total)
}

func totalScore(file *os.File, score map[byte]map[byte]int) int {
	total := 0
	for plays := range util.Lines(file) {
		them := plays[0]
		me := plays[2]
		total += score[them][me]
	}
	return total
}
