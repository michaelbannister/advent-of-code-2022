package util

import (
	"bufio"
	"fmt"
	"os"
)

func RunDay(day int, part1, part2 func(*os.File)) {
	path := fmt.Sprintf("inputs/day%d.txt", day)
	file, err := os.Open(path)
	check(err)
	defer file.Close()

	part1(file)

	file.Seek(0, 0)
	part2(file)
}

func Lines(file *os.File) chan string {
	c := make(chan string)

	scanner := bufio.NewScanner(file)

	go func() {
		for scanner.Scan() {
			value := scanner.Text()
			c <- value
		}
		close(c)
		return
	}()
	return c
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
