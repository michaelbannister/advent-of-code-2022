package main

import (
	"aoc2022/util"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func main() {
	util.RunDay(15, part1, part2)
}

func part1(file *os.File) {
	sensors := parseSensors(file)

	y := 2_000_000
	eliminated := eliminatedPoints(sensors, y)

	println("part 1:", len(eliminated))
}

func eliminatedPoints(sensors []sensor, y int) map[point]bool {
	eliminated := make(map[point]bool, 0)
	for _, s := range sensors {
		if r, found := s.coverageOnY(y); found {
			for x := r.from; x <= r.to; x++ {
				eliminated[point{x, y}] = true
			}
		}
	}

	for _, s := range sensors {
		delete(eliminated, s.beacon)
	}
	return eliminated
}

func parseSensors(file *os.File) []sensor {
	sensors := make([]sensor, 0)
	for line := range util.Lines(file) {
		coords := regexp.MustCompile("x=(-?\\d+), y=(-?\\d+)")
		matches := coords.FindAllStringSubmatch(line, 2)
		sensorX, _ := strconv.Atoi(matches[0][1])
		sensorY, _ := strconv.Atoi(matches[0][2])
		beaconX, _ := strconv.Atoi(matches[1][1])
		beaconY, _ := strconv.Atoi(matches[1][2])
		sensors = append(sensors, Sensor(sensorX, sensorY, beaconX, beaconY))
	}
	return sensors
}

func part2(file *os.File) {
	sensors := parseSensors(file)

	gap := findGap(sensors)

	println("part 2:", gap.x*4000000+gap.y)
}

func findGap(sensors []sensor) point {
	for y := 0; y <= 4_000_000; y++ {
		ranges := make([]xRange, 0)
		for _, s := range sensors {
			if r, found := s.coverageOnY(y); found {
				ranges = append(ranges, r)
			}
		}
		for x := 0; x < 4_000_000; x++ {
			possible := true
			for _, r := range ranges {
				if x >= r.from && x < r.to {
					x = r.to
					possible = false
					break
				}
			}
			if possible {
				return point{x, y}
			}
		}
	}
	return point{}
}

type point struct{ x, y int }
type xRange struct{ from, to int }

func (p point) String() string {
	return fmt.Sprintf("(%v,%v)", p.x, p.y)
}

func (p point) manhattanDistance(p1 point) int {
	return abs(p.x, p1.x) + abs(p.y, p1.y)
}

type sensor struct {
	location       point
	beacon         point
	beaconDistance int
}

func Sensor(x, y, beaconX, beaconY int) sensor {
	s := sensor{
		location: point{x, y},
		beacon:   point{beaconX, beaconY},
	}
	s.beaconDistance = s.location.manhattanDistance(s.beacon)
	return s
}

func (s *sensor) String() string {
	return fmt.Sprintf("sensor at %v, beacon at %v :: distance %v", s.location, s.beacon, s.beaconDistance)
}

func (s *sensor) coverageOnY(y int) (r xRange, found bool) {
	yDistance := abs(y, s.location.y)

	if yDistance > s.beaconDistance {
		return xRange{}, false
	}

	radius := s.beaconDistance - yDistance
	return xRange{s.location.x - radius, s.location.x + radius}, true
}

func abs(a, b int) int {
	if a < b {
		return b - a
	} else {
		return a - b
	}
}
