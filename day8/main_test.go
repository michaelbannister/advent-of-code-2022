package main

import (
	"reflect"
	"testing"
)

func Test_rotate(t *testing.T) {
	input := [][]byte{
		{'1', '2', '3'},
		{'4', '5', '6'},
	}
	want := [][]byte{
		{'3', '6'},
		{'2', '5'},
		{'1', '4'},
	}
	if got := rotate(input); !reflect.DeepEqual(got, want) {
		t.Errorf("rotate() = %v, want %v", got, want)
	}
}
