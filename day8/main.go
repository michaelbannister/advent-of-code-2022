package main

import (
	"aoc2022/util"
	"os"
)

func main() {
	util.RunDay(8, part1, part2)
}

type heightMap = [][]byte
type valueMap = [][]int

func part1(file *os.File) {
	heights := parseHeights(file)

	west, north, east, south := calculate(heights, visibility)

	or := func(a, b int) int { return a | b }
	vis := reduceArr(or, west, north, east, south)

	count := 0
	for i, row := range vis {
		for j := range row {
			count += vis[i][j]
		}
	}

	println("part 1:", count)
}

func part2(file *os.File) {
	heights := parseHeights(file)

	west, north, east, south := calculate(heights, viewingDistance)

	multiply := func(a, b int) int { return a * b }
	scores := reduceArr(multiply, west, north, east, south)

	max := 0
	for _, row := range scores {
		for _, score := range row {
			if score > max {
				max = score
			}
		}
	}

	println("part 1:", max)
}

func visibility(heights heightMap) valueMap {
	vis := *emptyMatrix[int](len(heights), len(heights[0]))
	for i, row := range heights {
		highest := byte('0') - 1
		for j, height := range row {
			if height > highest {
				highest = height
				vis[i][j] = 1
			} else {
				vis[i][j] = 0
			}
		}
	}
	return vis
}

func viewingDistance(heights heightMap) valueMap {
	distances := *emptyMatrix[int](len(heights), len(heights[0]))
	for i, row := range heights {
		for j, h := range row {
			distance := 0
			for t := j + 1; t < len(row); t++ {
				if row[t] == h {
					distance++
					break
				} else if row[t] < h {
					distance++
				} else {
					break
				}
			}
			distances[i][j] = distance
		}
	}
	return distances
}

func parseHeights(file *os.File) [][]byte {
	heights := make([][]byte, 0)

	for line := range util.Lines(file) {
		heights = append(heights, []byte(line))
	}
	return heights
}

func calculate(heights heightMap, calc func(heightMap) valueMap) (valueMap, valueMap, valueMap, valueMap) {
	west := calc(heights)
	north := rotateN(3, calc(rotateN(1, heights)))
	east := rotateN(2, calc(rotateN(2, heights)))
	south := rotateN(1, calc(rotateN(3, heights)))
	return west, north, east, south
}

func reduceArr[T any](reducer func(T, T) T, initial [][]T, vals ...[][]T) [][]T {
	if len(vals) == 0 {
		return initial
	}
	next := vals[0]
	for i, row := range initial {
		for j, value := range row {
			initial[i][j] = reducer(value, next[i][j])
		}
	}
	return reduceArr(reducer, initial, vals[1:]...)
}

func rotate[T int | byte](matrix [][]T) [][]T {
	d1 := len(matrix)
	d2 := len(matrix[0])
	rot := *emptyMatrix[T](d2, d1)

	for i, row := range matrix {
		for j, value := range row {
			rot[d2-1-j][i] = value
		}
	}

	return rot
}

func rotateN[T int | byte](n int, matrix [][]T) [][]T {
	var res = matrix
	for r := 0; r < n; r++ {
		res = rotate(res)
	}

	return res
}

func emptyMatrix[T int | byte](m, n int) *[][]T {
	mat := make([][]T, m)
	for i := 0; i < m; i++ {
		mat[i] = make([]T, n)
	}
	return &mat
}
