package main

import (
	"aoc2022/util"
	"fmt"
	"os"
	"strconv"
)

func main() {
	util.RunDay(9, part1, part2)
}

type point struct {
	x, y int
}

func (p point) plus(v vector) point {
	return point{p.x + v.x, p.y + v.y}
}
func (p point) move(dir byte) point {
	switch dir {
	case 'U':
		return p.plus(vector{0, 1})
	case 'R':
		return p.plus(vector{1, 0})
	case 'D':
		return p.plus(vector{0, -1})
	case 'L':
		return p.plus(vector{-1, 0})
	}
	panic("unknown direction: " + string(dir))
}

type rope struct {
	segments []segment
}

func (r rope) GetTail() point {
	return r.segments[len(r.segments)-1].tail
}

func (r rope) print() {
	for _, seg := range r.segments {
		fmt.Printf("(%v,%v)->(%v,%v) ", seg.head.x, seg.head.y, seg.tail.x, seg.tail.y)
	}
	println()
}

func (r rope) move(dir byte) rope {
	r.segments[0] = r.segments[0].move(dir)
	for i := 1; i < len(r.segments); i++ {
		r.segments[i] = r.segments[i].moveTo(r.segments[i-1].tail)
	}
	return r
}

func NewRope(size int) rope {
	segments := make([]segment, size)
	return rope{segments}
}

type segment struct {
	head, tail point
}

type vector struct {
	x, y int
}

func (s segment) vec() vector {
	return vector{x: s.head.x - s.tail.x, y: s.head.y - s.tail.y}
}

func (s segment) move(dir byte) segment {
	s.head = s.head.move(dir)
	return s.resolveTail()
}

func (s segment) moveTo(p point) segment {
	s.head = p
	return s.resolveTail()
}

func (s segment) resolveTail() segment {
	v := s.vec()
	switch v {
	case vector{0, 2}:
		s.tail = s.tail.move('U')
	case vector{2, 0}:
		s.tail = s.tail.move('R')
	case vector{0, -2}:
		s.tail = s.tail.move('D')
	case vector{-2, 0}:
		s.tail = s.tail.move('L')
	}
	if -1 <= v.x && v.x <= 1 && -1 <= v.y && v.y <= 1 {
		// adjacent
		return s
	}

	if v.x > 0 && v.y > 0 {
		s.tail = s.tail.move('U').move('R')
	} else if v.x > 0 && v.y < 0 {
		s.tail = s.tail.move('R').move('D')
	} else if v.x < 0 && v.y < 0 {
		s.tail = s.tail.move('D').move('L')
	} else if v.x < 0 && v.y > 0 {
		s.tail = s.tail.move('L').move('U')
	}
	return s
}

func part1(file *os.File) {
	r := NewRope(1)
	tailPath := make([]point, 1)
	tailPath[0] = r.GetTail()

	for line := range util.Lines(file) {
		dir := line[0]
		dist, _ := strconv.Atoi(line[2:])

		for i := 0; i < dist; i++ {
			r = r.move(dir)
			tailPath = append(tailPath, r.GetTail())
		}
	}

	tailPositions := make(map[point]int)
	for _, p := range tailPath {
		tailPositions[p]++
	}

	println("part 1:", len(tailPositions))
}

func part2(file *os.File) {
	r := NewRope(9)
	tailPath := make([]point, 1)
	tailPath[0] = r.GetTail()

	for line := range util.Lines(file) {
		dir := line[0]
		dist, _ := strconv.Atoi(line[2:])

		for i := 0; i < dist; i++ {
			r = r.move(dir)
			tailPath = append(tailPath, r.GetTail())
		}
	}

	tailPositions := make(map[point]int)
	for _, p := range tailPath {
		tailPositions[p]++
	}

	println("part 2:", len(tailPositions))
}
