package main

import (
	"aoc2022/util"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	util.RunDay(7, part1, part2)
}

func part1(file *os.File) {
	root := parseFs(file)

	dirs := root.DirsMatching(func(dir *Dir) bool {
		return dir.Size() <= 100_000
	})
	total := 0
	for _, dir := range dirs {
		total += dir.Size()
	}

	println("part 1:", total)
}

func part2(file *os.File) {
	root := parseFs(file)

	capacity := 70000000
	needFree := 30000000
	used := root.Size()
	free := capacity - used

	mustDelete := needFree - free

	dirs := root.DirsMatching(func(dir *Dir) bool {
		return dir.Size() >= mustDelete
	})

	smallest := capacity
	for _, dir := range dirs {
		if size := dir.Size(); size < smallest {
			smallest = size
		}
	}

	println("part 2:", smallest)
}

func parseFs(file *os.File) *Dir {
	ls := false
	root := &Dir{name: "/", nodes: []*Node{}}
	var currentDir *Dir
	for line := range util.Lines(file) {
		if ls && line[0] == '$' {
			ls = false
		}
		if !ls {
			cmd := line[2:4]
			if cmd == "cd" {
				dir := line[5:]
				if dir == "/" {
					currentDir = root
				} else if dir == ".." {
					currentDir = currentDir.parent
				} else {
					currentDir = currentDir.GetDir(dir)
				}
			} else if cmd == "ls" {
				ls = true
			} else {
				panic("unexpected command " + line)
			}
		} else {
			if line[0:3] == "dir" {
				currentDir.Mkdir(line[4:])
			} else {
				parts := strings.Split(line, " ")
				size, _ := strconv.Atoi(parts[0])
				name := parts[1]
				currentDir.CreateFile(name, size)
			}
		}
	}
	return root
}

type Node interface {
	Name() string
	Size() int
	Print(indent int)
}

type Dir struct {
	name   string
	nodes  []*Node
	parent *Dir
}

func (d *Dir) Name() string {
	return d.name
}

func (d *Dir) Size() int {
	size := 0
	for _, node := range d.nodes {
		size += (*node).Size()
	}
	return size
}

func (d *Dir) GetDir(name string) *Dir {
	for _, node := range d.nodes {
		if (*node).Name() == name {
			return (*node).(*Dir)
		}
	}
	return nil
}

func (d *Dir) Mkdir(name string) {
	dir := &Dir{
		name:   name,
		nodes:  []*Node{},
		parent: d,
	}
	dirAsNode := Node(dir)
	d.nodes = append(d.nodes, &dirAsNode)
}

func (d *Dir) CreateFile(name string, size int) {
	file := &File{name: name, size: size}
	fileAsNode := Node(file)
	d.nodes = append(d.nodes, &fileAsNode)
}

func (d *Dir) Print(indent int) {
	fmt.Printf("%s- %s (dir)\n", strings.Repeat("  ", indent), d.name)
	for _, node := range d.nodes {
		(*node).Print(indent + 1)
	}
}

func (d *Dir) DirsMatching(predicate func(*Dir) bool) []*Dir {
	result := make([]*Dir, 0)
	if predicate(d) {
		result = append(result, d)
	}
	for _, node := range d.nodes {
		switch n := (*node).(type) {
		case *Dir:
			result = append(result, n.DirsMatching(predicate)...)
		}
	}
	return result
}

type File struct {
	name string
	size int
}

func (f File) Name() string {
	return f.name
}

func (f File) Size() int {
	return f.size
}

func (f File) Print(indent int) {
	fmt.Printf("%s- %s (file, size=%d)\n", strings.Repeat("  ", indent), f.name, f.size)
}
