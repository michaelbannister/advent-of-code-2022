package main

import "testing"

func TestIntItem_CompareTo(t *testing.T) {
	tests := []struct {
		name  string
		left  IntItem
		right Item
		want  int
	}{
		{
			name:  "equal IntItem",
			left:  IntItem{1},
			right: Item(IntItem{1}),
			want:  0,
		},
		{
			name:  "bigger IntItem",
			left:  IntItem{1},
			right: Item(IntItem{3}),
			want:  -1,
		},
		{
			name:  "smaller IntItem",
			left:  IntItem{1},
			right: Item(IntItem{-3}),
			want:  1,
		},
		{
			name:  "equal single-element ListItem",
			left:  IntItem{1},
			right: ListItemOfInts(1),
			want:  0,
		},
		{
			name:  "bigger single-element ListItem",
			left:  IntItem{1},
			right: ListItemOfInts(4),
			want:  -1,
		},
		{
			name:  "smaller single-element ListItem",
			left:  IntItem{1},
			right: ListItemOfInts(-3),
			want:  1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.left.CompareTo(tt.right); got != tt.want {
				t.Errorf("CompareTo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestListItem_CompareTo(t *testing.T) {
	tests := []struct {
		name  string
		left  Item
		right Item
		want  int
	}{
		{
			name:  "equal IntItem",
			left:  ListItemOfInts(1),
			right: IntItem{1},
			want:  0,
		},
		{
			name:  "bigger IntItem",
			left:  ListItemOfInts(1),
			right: Item(IntItem{3}),
			want:  -1,
		},
		{
			name:  "smaller IntItem",
			left:  ListItemOfInts(1),
			right: Item(IntItem{-3}),
			want:  1,
		},
		{
			name:  "equal ListItem",
			left:  ListItemOfInts(1),
			right: ListItemOfInts(1),
			want:  0,
		},
		{
			name:  "bigger single-element ListItem",
			left:  ListItemOfInts(1),
			right: ListItemOfInts(2),
			want:  -1,
		},
		{
			name:  "smaller single-element ListItem",
			left:  ListItemOfInts(1),
			right: ListItemOfInts(-4),
			want:  1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.left.CompareTo(tt.right); got != tt.want {
				t.Errorf("CompareTo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExamples(t *testing.T) {
	tests := []struct {
		name  string
		left  Item
		right Item
		want  int
	}{
		{
			name:  "Pair 1",
			left:  ListItemOfInts(1, 1, 3, 1, 1),
			right: ListItemOfInts(1, 1, 5, 1, 1),
			want:  -1,
		},
		{
			name:  "Pair 2",
			left:  ListItem{[]Item{ListItemOfInts(1), ListItemOfInts(2, 3, 4)}},
			right: ListItem{[]Item{ListItemOfInts(1), IntItemOf(4)}},
			want:  -1,
		},
		{
			name:  "Pair 3",
			left:  ListItemOfInts(9),
			right: ListItemOfItems(ListItemOfInts(8, 7, 6)),
			want:  1,
		},
		{
			name:  "Pair 4",
			left:  ListItemOfItems(ListItemOfInts(4, 4), IntItemOf(4), IntItemOf(4)),
			right: ListItemOfItems(ListItemOfInts(4, 4), IntItemOf(4), IntItemOf(4), IntItemOf(4)),
			want:  -1,
		},
		{
			name:  "Pair 5",
			left:  ListItemOfInts(7, 7, 7, 7),
			right: ListItemOfInts(7, 7, 7),
			want:  1,
		},
		{
			name:  "Pair 6",
			left:  EmptyList(),
			right: ListItemOfInts(3),
			want:  -1,
		},
		{
			name:  "Pair 7",
			left:  ListItemOfItems(ListItemOfItems(EmptyList())),
			right: ListItemOfItems(EmptyList()),
			want:  1,
		},
		{
			name: "Pair 8",
			left: ListItemOfItems(
				IntItemOf(1),
				ListItemOfItems(
					IntItemOf(2),
					ListItemOfItems(
						IntItemOf(3),
						ListItemOfItems(
							IntItemOf(4),
							ListItemOfInts(5, 6, 7),
						),
					),
				),
				IntItemOf(8),
				IntItemOf(9),
			),
			right: ListItemOfItems(
				IntItemOf(1),
				ListItemOfItems(
					IntItemOf(2),
					ListItemOfItems(
						IntItemOf(3),
						ListItemOfItems(
							IntItemOf(4),
							ListItemOfInts(5, 6, 0), // the zero is different
						),
					),
				),
				IntItemOf(8),
				IntItemOf(9),
			),
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.left.CompareTo(tt.right); got != tt.want {
				t.Errorf("CompareTo() = %v, want %v", got, tt.want)
			}
		})
	}
}
