package main

import (
	"aoc2022/util"
	"bufio"
	"fmt"
	"os"
	"reflect"
	"sort"
	"strconv"
	"strings"
)

func main() {
	util.RunDay(13, part1, part2)
}

func part1(file *os.File) {
	scanner := bufio.NewScanner(file)

	pairs := parseInput(scanner)
	count := 0
	for i, pair := range pairs {
		if pair.left.CompareTo(pair.right) == -1 {
			count += i + 1
		}
	}

	println("part 1:", count)
}

func part2(file *os.File) {
	packets := make([]ListItem, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if text := scanner.Text(); len(text) > 0 {
			packets = append(packets, parsePacket(text))
		}
	}
	divider1 := ListItemOfItems(ListItemOfInts(2)).(ListItem)
	divider2 := ListItemOfItems(ListItemOfInts(6)).(ListItem)
	packets = append(packets, divider1, divider2)

	sort.Slice(packets, func(i, j int) bool {
		return packets[i].CompareTo(packets[j]) == -1
	})

	product := 1

	for i, packet := range packets {
		if reflect.DeepEqual(packet, divider1) || reflect.DeepEqual(packet, divider2) {
			product *= i + 1
		}
	}

	println("part 2:", product)
}

func parseInput(scanner *bufio.Scanner) []PacketPair {
	pairs := make([]PacketPair, 0)
	for scanner.Scan() {
		left := parsePacket(scanner.Text())
		scanner.Scan()
		right := parsePacket(scanner.Text())
		scanner.Scan()
		pairs = append(pairs, PacketPair{left, right})
	}
	return pairs
}

func parsePacket(text string) ListItem {
	list, rest := parseList(text[1:])
	if len(rest) > 0 {
		panic("we've gone wrong, remaining: " + rest)
	}
	return list
}

func parseList(text string) (ListItem, string) {
	list := make([]Item, 0)
	for true {
		if text[0] == ']' {
			text = text[1:]
			return ListItem{list}, text
		} else if text[0] == '[' {
			innerList, rest := parseList(text[1:])
			list = append(list, innerList)
			text = rest
		} else if text[0] == ',' {
			text = text[1:]
		} else {
			nextToken := strings.IndexAny(text, ",[]")
			number, _ := strconv.Atoi(text[:nextToken])
			intItem := IntItem{number}
			list = append(list, intItem)
			text = text[nextToken:]
		}
	}
	return ListItem{list}, text
}

type PacketPair struct {
	left, right ListItem
}

type Item interface {
	fmt.Stringer
	CompareTo(Item) int
}

type IntItem struct {
	value int
}

func (s IntItem) CompareTo(other Item) int {
	switch right := other.(type) {
	case IntItem:
		if s.value == right.value {
			return 0
		} else if s.value < right.value {
			return -1
		} else {
			return 1
		}
	case ListItem:
		return ListItemOfItems(s).CompareTo(other)
	}
	panic("I wouldn't have this problem with Kotlin…")
}

func (s IntItem) String() string {
	return strconv.Itoa(s.value)
}

type ListItem struct {
	value []Item
}

func (l ListItem) CompareTo(other Item) int {
	switch right := other.(type) {
	case IntItem:
		return -right.CompareTo(l)
	case ListItem:
		for i, item := range l.value {
			if i >= len(right.value) {
				return 1
			}
			comp := item.CompareTo(right.value[i])
			if comp != 0 {
				return comp
			}
		}
		if len(l.value) < len(right.value) {
			return -1
		} else {
			return 0
		}
	}
	panic("I wouldn't have this problem with Kotlin…")
}

func (l ListItem) String() string {
	s := "["
	for _, item := range l.value {
		s += item.String() + ","
	}

	return strings.TrimSuffix(s, ",") + "]"
}

func IntItemOf(i int) Item { return IntItem{i} }
func ListItemOfInts(ints ...int) Item {
	list := make([]Item, len(ints))
	for i, value := range ints {
		it := IntItem{value}
		list[i] = Item(it)
	}
	return Item(ListItem{list})
}

func ListItemOfItems(items ...Item) Item {
	return ListItem{items}
}

func EmptyList() Item {
	return ListItem{}
}
