package main

import (
	"aoc2022/util"
	"os"
	"regexp"
	"strconv"
)

func main() {
	util.RunDay(4, part1, part2)
}

func part1(file *os.File) {
	count := 0
	for assignments := range util.Lines(file) {
		a, b := parse(assignments)

		if a.contains(b) || b.contains(a) {
			count++
		}
	}

	println("part 1:", count)
}

func part2(file *os.File) {
	count := 0
	for assignments := range util.Lines(file) {
		a, b := parse(assignments)

		if a.overlaps(b) {
			count++
		}
	}

	println("part 2:", count)
}

var matcher = regexp.MustCompile("(\\d+)-(\\d+),(\\d+)-(\\d+)")

func parse(assignments string) (*SectionRange, *SectionRange) {
	matches := matcher.FindStringSubmatch(assignments)
	left1, _ := strconv.Atoi(matches[1])
	right1, _ := strconv.Atoi(matches[2])
	left2, _ := strconv.Atoi(matches[3])
	right2, _ := strconv.Atoi(matches[4])
	return &SectionRange{left: left1, right: right1},
		&SectionRange{left: left2, right: right2}
}

type SectionRange struct {
	left, right int
}

func (s *SectionRange) contains(other *SectionRange) bool {
	return s.left <= other.left && other.right <= s.right
}

func (s *SectionRange) overlaps(other *SectionRange) bool {
	return other.left <= s.right && s.left <= other.right
}
