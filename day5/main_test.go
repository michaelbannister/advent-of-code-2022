package main

import (
	"reflect"
	"testing"
)

func TestParseInitialState(t *testing.T) {
	input := "" +
		"    [D]    \n" +
		"[N] [C]    \n" +
		"[Z] [M] [P]\n" +
		" 1   2   3 "
	expected := State{
		stacks: [][]byte{
			{'Z', 'N'},
			{'M', 'C', 'D'},
			{'P'},
		},
	}

	actual := *parseInitialState(input)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("got %v, expected %v", actual, expected)
	}
}

func TestParseMoves(t *testing.T) {
	input := "move 1 from 2 to 1\n" +
		"move 13 from 1 to 3"

	expected := []Move{
		{
			quantity: 1,
			from:     2,
			to:       1,
		},
		{
			quantity: 13,
			from:     1,
			to:       3,
		},
	}

	actual := parseMoves(input)
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("got %+v, expected %+v", actual, expected)
	}
}

func TestApplyMove(t *testing.T) {
	state := State{
		stacks: [][]byte{
			{'A', 'B'},
			{'C'},
		},
	}

	state.apply(Move{quantity: 2, from: 1, to: 2})

	expected := State{
		stacks: [][]byte{
			{},
			{'C', 'B', 'A'},
		},
	}

	if !reflect.DeepEqual(state, expected) {
		t.Errorf("got %+v, expected %+v", state, expected)
	}
}

func TestApplyMoveMulti(t *testing.T) {
	state := State{
		stacks: [][]byte{
			{'A', 'B'},
			{'C'},
		},
		moveMulti: true,
	}

	state.apply(Move{quantity: 2, from: 1, to: 2})

	expected := State{
		stacks: [][]byte{
			{},
			{'C', 'A', 'B'},
		},
		moveMulti: true,
	}

	if !reflect.DeepEqual(state, expected) {
		t.Errorf("got %+v, expected %+v", state, expected)
	}
}
