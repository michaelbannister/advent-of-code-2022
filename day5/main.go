package main

import (
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	b, err := os.ReadFile("inputs/day5.txt")
	if err != nil {
		panic(err)
	}

	content := string(b)

	parts := strings.Split(content, "\n\n")
	initialSpec := parts[0]
	movesSpec := parts[1]

	part1(initialSpec, movesSpec)
	part2(initialSpec, movesSpec)
}

func part1(initialSpec string, movesSpec string) {
	state := parseInitialState(initialSpec)
	moves := parseMoves(movesSpec)

	for _, move := range moves {
		state.apply(move)
	}

	println("part 1:", state.readTop())
}

func part2(initialSpec string, movesSpec string) {
	state := parseInitialState(initialSpec)
	moves := parseMoves(movesSpec)
	state.moveMulti = true

	for _, move := range moves {
		state.apply(move)
	}

	println("part 2:", state.readTop())
}

func parseInitialState(input string) *State {
	lines := strings.Split(input, "\n")

	reLastNum := regexp.MustCompile("(\\d+)\\s*$")
	lastNum := reLastNum.FindStringSubmatch(lines[len(lines)-1])[1]
	numStacks, _ := strconv.Atoi(lastNum)
	stacks := make([][]byte, numStacks)

	for i := len(lines) - 2; i >= 0; i-- {
		line := lines[i]
		for pos, stk := 0, 0; pos < len(line); pos, stk = pos+4, stk+1 {
			box := line[pos : pos+2]
			if box[0] == '[' {
				stacks[stk] = append(stacks[stk], box[1])
			}
		}
	}
	return &State{
		stacks: stacks,
	}
}

func parseMoves(input string) []Move {
	moveRe := regexp.MustCompile("move (\\d+) from (\\d+) to (\\d+)")

	lines := strings.Split(input, "\n")
	moves := make([]Move, len(lines))
	for i, line := range lines {
		values := moveRe.FindStringSubmatch(line)
		quantity, _ := strconv.Atoi(values[1])
		from, _ := strconv.Atoi(values[2])
		to, _ := strconv.Atoi(values[3])
		moves[i] = Move{
			quantity: uint(quantity),
			from:     uint(from),
			to:       uint(to),
		}
	}
	return moves
}

type State struct {
	stacks    [][]byte
	moveMulti bool
}

func (s State) apply(move Move) {
	if s.moveMulti {
		crates := s.take(move.from, move.quantity)
		s.drop(move.to, crates)
	} else {
		for i := 0; i < int(move.quantity); i++ {
			crate := s.pop(move.from)
			s.push(move.to, crate)
		}
	}
}

func (s State) pop(from uint) byte {
	idx := from - 1
	stack := s.stacks[idx]
	top := len(stack) - 1
	crate := stack[top]
	s.stacks[idx] = stack[:top]
	return crate
}

func (s State) push(to uint, crate byte) {
	idx := to - 1
	s.stacks[idx] = append(s.stacks[idx], crate)
}

func (s State) readTop() string {
	tops := make([]byte, len(s.stacks))
	for i, stack := range s.stacks {
		if len(stack) == 0 {
			tops[i] = ' '
		} else {
			tops[i] = stack[len(stack)-1]
		}
	}
	return string(tops)
}

func (s State) take(from, quantity uint) []byte {
	idx := from - 1
	stack := s.stacks[idx]
	moveIdx := len(stack) - int(quantity)
	crates := stack[moveIdx:]
	s.stacks[idx] = stack[:moveIdx]
	return crates
}

func (s State) drop(to uint, crates []byte) {
	idx := to - 1
	s.stacks[idx] = append(s.stacks[idx], crates...)
}

type Move struct {
	quantity, from, to uint
}
