package main

import (
	"aoc2022/util"
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	util.RunDay(11, part1, part2)
}

type Monkey struct {
	items          []int
	op             func(int) int
	test           int
	throwToIfTrue  int
	throwToIfFalse int
	count          int
}

func (m *Monkey) inspectItems(monkeys []*Monkey, reduceWorry func(int) int) {
	for _, item := range m.items {
		m.count++
		worry := m.op(item)
		worry = reduceWorry(worry)
		if worry%m.test == 0 {
			monkeys[m.throwToIfTrue].catch(worry)
		} else {
			monkeys[m.throwToIfFalse].catch(worry)
		}
	}
	m.items = nil
}

func (m *Monkey) catch(item int) {
	m.items = append(m.items, item)
}

func parseMonkeys(file *os.File) []*Monkey {
	monkeys := make([]*Monkey, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		monkey := &Monkey{}

		// skip Monkey header

		scanner.Scan()
		monkey.items = parseStartingItems(scanner.Text())

		scanner.Scan()
		monkey.op = parseOperation(scanner.Text())

		scanner.Scan()
		monkey.test = parseTest(scanner.Text())

		// assume sequence of true then false
		scanner.Scan()
		monkey.throwToIfTrue = parseThrowTo(scanner.Text())

		scanner.Scan()
		monkey.throwToIfFalse = parseThrowTo(scanner.Text())

		scanner.Scan() // skip empty line
		monkeys = append(monkeys, monkey)
	}
	return monkeys
}

func parseStartingItems(line string) []int {
	itemsStr := strings.Split(strings.TrimPrefix(line, "  Starting items: "), ", ")
	items := make([]int, len(itemsStr))
	for i, item := range itemsStr {
		val, _ := strconv.Atoi(item)
		items[i] = val
	}
	return items
}

func parseOperation(text string) func(int) int {
	re := regexp.MustCompile("  Operation: new = old ([*+]) (\\d+|old)")
	match := re.FindStringSubmatch(text)
	op := match[1]
	operand := match[2]
	if operand == "old" {
		if op == "*" {
			return func(old int) int { return old * old }
		} else if op == "+" {
			return func(old int) int { return old + old }
		}
	} else {
		num, _ := strconv.Atoi(operand)
		if op == "*" {
			return func(old int) int { return old * num }
		} else if op == "+" {
			return func(old int) int { return old + num }
		}
	}
	panic("failed to parse operation " + text)
}

func parseTest(text string) int {
	re := regexp.MustCompile("divisible by (\\d+)")
	match := re.FindStringSubmatch(text)
	divisor, _ := strconv.Atoi(match[1])
	return divisor
}

func parseThrowTo(text string) int {
	num, _ := strconv.Atoi(strings.Split(text, "throw to monkey ")[1])
	return num
}

func productTop2(in []int) int64 {
	t1 := 0
	t2 := 0
	for _, val := range in {
		if val > t1 {
			t2 = t1
			t1 = val
		} else if val > t2 {
			t2 = val
		}
	}
	return int64(t1) * int64(t2)
}

func printInspections(monkeys []*Monkey) {
	for i, m := range monkeys {
		fmt.Printf("Monkey %v inspected items %v times\n", i, m.count)
	}
	println()
}
func part1(file *os.File) {
	monkeys := parseMonkeys(file)

	for r := 0; r < 20; r++ {
		for _, monkey := range monkeys {
			monkey.inspectItems(monkeys, func(w int) int { return w / 3 })
		}
	}

	inspections := make([]int, len(monkeys))
	for i, m := range monkeys {
		inspections[i] = m.count
	}

	result := productTop2(inspections)

	println("part 1:", result)
}

func part2(file *os.File) {
	monkeys := parseMonkeys(file)
	testCommonMultiple := 1
	for _, m := range monkeys {
		testCommonMultiple *= m.test
	}

	for r := 0; r < 10000; r++ {
		for _, monkey := range monkeys {
			monkey.inspectItems(monkeys, func(w int) int {
				return w % testCommonMultiple
			})
		}
	}

	printInspections(monkeys)

	inspections := make([]int, len(monkeys))
	for i, m := range monkeys {
		inspections[i] = m.count
	}

	result := productTop2(inspections)

	println("part 2:", result)
}
