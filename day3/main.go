package main

import (
	"aoc2022/util"
	"bufio"
	"os"
	"strings"
)

func main() {
	util.RunDay(3, part1, part2)
}

func part1(file *os.File) {
	scanner := bufio.NewScanner(file)

	sum := 0
	for scanner.Scan() {
		rucksack := scanner.Text()
		comp1, comp2 := split(rucksack)

		commonItem := common(comp1, comp2)
		sum += priority(commonItem)
	}

	println("part 1:", sum)
}

func part2(file *os.File) {
	scanner := bufio.NewScanner(file)

	sum := 0
	for scanner.Scan() {
		first := scanner.Text()
		scanner.Scan()
		second := scanner.Text()
		scanner.Scan()
		third := scanner.Text()

		badge := common3(first, second, third)
		sum += priority(badge)
	}
	println("part 2:", sum)
}

func priority(item byte) int {
	if 'a' <= item && item <= 'z' {
		return int(item - 'a' + 1)
	}
	if 'A' <= item && item <= 'Z' {
		return int(item - 'A' + 27)
	}
	panic("unknown priority")
}

func split(rucksack string) (string, string) {
	midpoint := len(rucksack) / 2
	return rucksack[:midpoint], rucksack[midpoint:]
}

func common(a string, b string) byte {
	for _, item := range a {
		if strings.ContainsRune(b, item) {
			return byte(item)
		}
	}
	panic("nothing in common")
}

func common3(a, b, c string) byte {
	for _, item := range a {
		if strings.ContainsRune(b, item) && strings.ContainsRune(c, item) {
			return byte(item)
		}
	}
	panic("nothing in common")
}
