package main

import (
	"aoc2022/util"
	"os"
	"strconv"
)

func main() {
	util.RunDay(1, part1, part2)
}

func part1(file *os.File) {
	mostCalories := 0

	for calories := range elfCalories(file) {
		mostCalories = max(mostCalories, calories)
	}

	println("part 1:", mostCalories)
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func part2(file *os.File) {
	top3 := []int{0, 0, 0}

	for calories := range elfCalories(file) {
		leastIndex := indexOfLeast(top3)
		if calories > top3[leastIndex] {
			top3[leastIndex] = calories
		}
	}

	total := 0
	for _, calories := range top3 {
		total += calories
	}
	println("part 2:", total)
}

func elfCalories(file *os.File) chan int {
	c := make(chan int)

	go func() {
		accumulator := 0
		for line := range util.Lines(file) {
			if len(line) != 0 {
				itemCalories, err := strconv.Atoi(line)
				check(err)
				accumulator += itemCalories
			} else {
				c <- accumulator
				accumulator = 0
			}
		}
		c <- accumulator
		close(c)
		return
	}()
	return c
}

func indexOfLeast(array []int) int {
	leastIdx := 0
	for idx, value := range array {
		if value < array[leastIdx] {
			leastIdx = idx
		}
	}
	return leastIdx
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
