package main

import (
	"aoc2022/util"
	"bufio"
	"os"
)

func main() {
	util.RunDay(6, part1, part2)
}

func part1(file *os.File) {
	pos := endOfUniqueSeq(file, 4)

	println("part 1:", pos)
}

func part2(file *os.File) {
	pos := endOfUniqueSeq(file, 14)

	println("part 2:", pos)
}

func endOfUniqueSeq(file *os.File, length int) int {
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanBytes)

	pos := 0
	buffer := make([]byte, length)
	for scanner.Scan() {
		b := scanner.Bytes()[0]
		pos++
		for i, b2 := range buffer {
			if b == b2 {
				// reset the buffer starting _after_ the existing byte which matches the new byte
				buffer = buffer[i+1:]
				break
			}
		}
		buffer = append(buffer, b)
		if len(buffer) == length {
			break
		}
	}
	return pos
}
